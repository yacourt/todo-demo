import { PrismaClient } from "@prisma/client";
import { faker } from "@faker-js/faker";
const prisma = new PrismaClient();

function generateTask() {
  return {
    task: faker.lorem.sentence(),
    completed: false,
  };
}

async function seed() {
  for (let i = 0; i < 10; i++) {
    const task = generateTask();
    await prisma.todo.create({ data: task });
  }
}

seed()
  .catch((error) => {
    console.error(error);
    process.exit(1);
  })
  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  .finally(async () => {
    await prisma.$disconnect();
  });

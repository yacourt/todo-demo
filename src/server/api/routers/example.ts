import { z } from "zod";

import { createTRPCRouter, publicProcedure } from "~/server/api/trpc";

export const exampleRouter = createTRPCRouter({
  hello: publicProcedure
    .input(z.object({ text: z.string() }))
    .query(({ input }) => {
      return {
        greeting: `Hello ${input.text}`,
      };
    }),
  getAll: publicProcedure.query(async ({ ctx }) => {
    const result = await ctx.prisma.todo.findMany();
    return result;
  }),
  createOne: publicProcedure
    .input(z.object({ task: z.string() }))
    .mutation(async ({ ctx, input }) => {
      const result = await ctx.prisma.todo.create({ data: input });
      return result;
    }),
  updateOne: publicProcedure
    .input(
      z.object({
        id: z.string(),
        task: z.string().optional(),
        completed: z.boolean().optional(),
      })
    )
    .mutation(async ({ ctx, input }) => {
      const { id, ...data } = input;
      const result = await ctx.prisma.todo.update({
        where: { id },
        data: data,
      });
      return result;
    }),
  deleteOne: publicProcedure
    .input(z.object({ id: z.string() }))
    .mutation(async ({ ctx, input }) => {
      const { id } = input;
      await ctx.prisma.todo.delete({ where: { id } });
      return null;
    }),
});

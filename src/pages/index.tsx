import { type NextPage } from "next";
import Head from "next/head";
import { useForm } from "react-hook-form";
import { useQueryClient } from "@tanstack/react-query";

import { api, type RouterInputs } from "~/utils/api";
import { getQueryKey } from "@trpc/react-query";
import FlipMove from "react-flip-move";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>Create Todo App</title>
        <meta name="description" content="Generated by create-t3-app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <ToastContainer />
      <main className="flex flex-col items-center justify-center">
        <div className="order-first pt-10 pb-10">
          <TodoForm />
        </div>
        <TodoTable />
      </main>
    </>
  );
};

interface Props {
  headerNames: string[];
}

const TableHeader = ({ headerNames }: Props) => (
  <thead>
    <tr>
      {headerNames.map((name) => (
        <th key={name} className="px-4 py-2">
          {name}
        </th>
      ))}
    </tr>
  </thead>
);

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  children: React.ReactNode;
}
const Button = ({ children, ...restProps }: ButtonProps) => {
  return (
    <button
      className="rounded bg-blue-500 py-2 px-4 font-bold text-white hover:bg-blue-700"
      {...restProps}
    >
      {children}
    </button>
  );
};

type UpdateOneTaskPayload = RouterInputs["example"]["updateOne"];
type CreateOneTaskPayload = RouterInputs["example"]["createOne"];

function TodoForm() {
  const { register, reset, handleSubmit } = useForm<CreateOneTaskPayload>();
  const queryClient = useQueryClient();

  const createTodo = api.example.createOne.useMutation({
    onSuccess: async () => {
      await queryClient.invalidateQueries(getQueryKey(api.example.getAll));
      reset();
    },
  });

  const onSubmit = async (data: CreateOneTaskPayload) => {
    await createTodo.mutateAsync(data);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="flex flex-row space-x-2">
      <input
        type="text"
        {...register("task")}
        className="rounded-md 
       border border-gray-300 py-2 px-4 focus:border-blue-500 focus:outline-none
       focus:ring-1 focus:ring-blue-500"
      />
      <Button
        type="submit"
        className="rounded bg-blue-500 py-2 px-4
       font-bold text-white hover:bg-blue-700"
      >
        Add
      </Button>
    </form>
  );
}

interface DataProps extends React.TableHTMLAttributes<HTMLTableCellElement> {
  children: React.ReactNode;
}

const TableData = ({ children, ...restProps }: DataProps) => (
  <td
    {...restProps}
    className="border px-4 py-2"
    suppressContentEditableWarning={true}
  >
    {children}
  </td>
);

function TodoTable() {
  const queryClient = useQueryClient();
  const todos = api.example.getAll.useQuery().data;
  const notifyTodoUpdated = () => toast.success("Todo updated!");
  const notifyError = () => toast.error("Failed to update todo!");

  const headerNames = ["Task", "Completed", "Created At", "Actions"];

  const RemoteActions = {
    deleteTodo: api.example.deleteOne.useMutation({
      onSuccess: async () => {
        await queryClient.invalidateQueries(getQueryKey(api.example.getAll));
      },
    }),
    updateTodo: api.example.updateOne.useMutation({
      onSuccess: async () => {
        await queryClient.invalidateQueries(getQueryKey(api.example.getAll));
        notifyTodoUpdated();
      },
      onError: (error) => {
        notifyError();
        console.error(error.data);
      },
    }),
  };

  const Handlers = {
    onUpdate: async (data: UpdateOneTaskPayload) => {
      await RemoteActions.updateTodo.mutateAsync(data);
    },
    onDelete: async (id: string) => {
      await RemoteActions.deleteTodo.mutateAsync({ id });
    },
  };

  if (!todos) {
    return <div>Loading todos</div>;
  }

  return (
    <table className="table-auto">
      <TableHeader headerNames={headerNames} />
      <FlipMove typeName="tbody">
        {todos.map((todo) => {
          const taskNameId = todo.id + "-taskName";
          const taskCompletedId = todo.id + "-taskCompleted";
          const getTaskNameValue = () =>
            document.getElementById(todo.id + "-taskName")?.innerText;
          const getTaskCompletionValue = () =>
            document.getElementById(todo.id + "-taskCompleted")?.innerText;

          return (
            <tr id={todo.id} key={todo.id}>
              <TableData
                id={taskNameId}
                contentEditable
                onBlur={async () => {
                  if (getTaskNameValue() !== todo.task) {
                    await Handlers.onUpdate({
                      ...todo,
                      task: getTaskNameValue() || todo.task,
                    });
                  }
                }}
              >
                {todo.task}
              </TableData>
              <TableData>
                <input
                  id={taskCompletedId}
                  defaultChecked={todo.completed}
                  type="checkbox"
                  onClick={() =>
                    Handlers.onUpdate({
                      ...todo,
                      completed:
                        Boolean(getTaskCompletionValue()) || todo.completed,
                    })
                  }
                ></input>
              </TableData>
              <TableData>{new Date(todo.createdAt).toLocaleString()}</TableData>
              <TableData>
                <Button onClick={() => Handlers.onDelete(todo.id)}>
                  Delete
                </Button>
              </TableData>
            </tr>
          );
        })}
      </FlipMove>
    </table>
  );
}

export default Home;

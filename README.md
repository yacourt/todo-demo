Une application qui présente la stack prisma + trpc + Next pour réaliser une petite todo app (~300LOC).

Pour faire un petit tour du propriétaire :

- [Le front](src/pages/index.tsx)
- [Le schéma](prisma/schema.prisma)
- [L'api](src/server/api/routers/example.ts).

Vous pouvez aussi repasser les commits.

L'idée c'est qu'il n'y a pas d'état local à garder synchro (pas de useState, de useEffect). React-query s'occupe de gérer les données du serveur. Les données sont stockées dans le cache dans un dictionnaire clé/valeur et sont accessibles par tous les composants. Pour le reste (input utilisateur, form) on utilise le DOM. Cela simplifie grandement la logique.
L'application est typée de bout en bout, ça veut dire que je peux réutiliser les types déclarés côtés serveur dans mon front. Ces types sont définis par des parsers écrits avec Zod. On peut les générer directement depuis le schéma prisma si on veut gagner du temps. Grâce à ces parseurs il sera aisé d'ajouter la validation côté client pour le micro formulaire de soumission des tâches (par exemple en vérifiant l'unicité du nom).
Cela est utile à la robustesse du système, à la prévention des erreurs, à s'assurer que le front reste en synchro avec le schéma des données, à éviter la duplication de code entre front et back, et permet d'avoir des annotations et de l'autocomplétion.

Parmi les autres librairies intéressantes à noter :

- faker js pour générer des données facilement.
- react-flip-move, qui permet d'animer une liste de children en utilisant la technique du flip (https://aerotwist.com/blog/flip-your-animations/#the-general-approach).
- toastify pour les notifs. Une librairie facile à utiliser.
- tailwind pour le style.

Pour démarrer :

- `npm i`
- `npx prisma db push`
- `npx prisma db seed`
- `npm run dev`

Pour explorer la bdd :

- `npx prisma studio`

Pour explorer la bdd
`npx prisma studio`

```mermaid
graph TD;

    SQLite -. Provides data .-> Prisma;
    Prisma -. Zod parses Output .-> TRPC;
    Prisma -. Generates Schema .-> Zod;
    TRPC --> ReactQuery;
    Zod --> TRPC
    Zod -. Provides types for prisma entities .-> NextJS
    ReactQuery -. Maintain state .-> NextJS;

    NextJS -. Requires data .-> ReactQuery;
    ReactQuery --> TRPC;
    TRPC -. Zod parses Input .-> Prisma;
    Prisma -. Models .-> SQLite;

    style TRPC stroke:#2ecc71,stroke-width:2px;
    style Prisma stroke:#3498db,stroke-width:2px;
    style SQLite stroke:#95a5a6,stroke-width:2px;
    style NextJS stroke:#f1c40f,stroke-width:2px;
    style Zod stroke:#9b59b6,stroke-width:2px;
    click TRPC "https://trpc.io"
    click Prisma "https://www.prisma.io"
    click SQLite "https://sqlite.org/"
    click ReactQuery "https://react-query.tanstack.com/"
    click NextJS "https://nextjs.org/"
    click Zod "https://github.com/vriad/zod"
```
